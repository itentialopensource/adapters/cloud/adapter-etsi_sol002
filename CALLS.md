## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for ETSI Sol002. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for ETSI Sol002.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the ETSI Standard sol002. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getApiVersions(callback)</td>
    <td style="padding:15px">Retrieve API version information</td>
    <td style="padding:15px">{base_path}/{version}/api_versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfiguration(callback)</td>
    <td style="padding:15px">Read VNF/VNFC configuration from VNF</td>
    <td style="padding:15px">{base_path}/{version}/vnfconfig/v1/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchConfiguration(body, callback)</td>
    <td style="padding:15px">Modify VNF/VNFC configuration.</td>
    <td style="padding:15px">{base_path}/{version}/vnfconfig/v1/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndicators(filter, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query multiple indicators</td>
    <td style="padding:15px">{base_path}/{version}/vnfind/v1/indicators?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndicatorsVnfInstanceId(vnfInstanceId, filter, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query multiple indicators related to a VNF instance.</td>
    <td style="padding:15px">{base_path}/{version}/vnfind/v1/indicators/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIndicatorsVnfInstanceIdIndicatorId(vnfInstanceId, indicatorId, callback)</td>
    <td style="padding:15px">Read an inidividual VNF indicator related to a VNF instance.</td>
    <td style="padding:15px">{base_path}/{version}/vnfind/v1/indicators/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubscriptions(filter, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">Query multiple subscriptions.</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSubscriptions(body, callback)</td>
    <td style="padding:15px">Create a new subscription to VNF indicator change notifications</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/subscriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubscriptionsSubscriptionId(subscriptionId, callback)</td>
    <td style="padding:15px">Read an individual subscription.</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/subscriptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubscriptionsSubscriptionId(subscriptionId, callback)</td>
    <td style="padding:15px">Delete a subscription</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/subscriptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarms(filter, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">The client can use this method to retrieve information about the alarm list.
</td>
    <td style="padding:15px">{base_path}/{version}/vnffm/v1/alarms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlarmsAlarmId(alarmId, callback)</td>
    <td style="padding:15px">The client can use this method to read an individual alarm.
</td>
    <td style="padding:15px">{base_path}/{version}/vnffm/v1/alarms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAlarmsAlarmId(alarmId, body, callback)</td>
    <td style="padding:15px">This method modifies an individual alarm resource.
</td>
    <td style="padding:15px">{base_path}/{version}/vnffm/v1/alarms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAlarmsAlarmIdEscalate(alarmId, body, callback)</td>
    <td style="padding:15px">The POST method enables the consumer to escalate the perceived severity of an alarm that is represe</td>
    <td style="padding:15px">{base_path}/{version}/vnffm/v1/alarms/{pathv1}/escalate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfInstances(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">The GET method queries information about multiple VNF instances.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstances(body, callback)</td>
    <td style="padding:15px">The POST method creates a new VNF instance resource based on a VNF package that is onboarded and in</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfInstancesVnfInstanceId(vnfInstanceId, callback)</td>
    <td style="padding:15px">Information about a VNF instance by reading an "Individual VNF instance".
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVnfInstancesVnfInstanceId(vnfInstanceId, callback)</td>
    <td style="padding:15px">This method deletes an "Individual VNF instance" resource.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVnfInstancesVnfInstanceId(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">This method modifies an "Individual VNF instance" resource. Changes to the VNF configurable propert</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdInstantiate(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method instantiates a VNF instance.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/instantiate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdScale(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method requests to scale a VNF instance resource incrementally.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdScaleToLevel(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method requests to scale a VNF instance resource to a target level.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/scale_to_level?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdChangeFlavour(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method changes the deployment flavour of a VNF instance.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/change_flavour?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdTerminate(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method triggers the VNFM to terminate a VNF  instance and to request to the VIM the releas</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/terminate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdHeal(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method requests to heal a VNF instance.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/heal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdOperate(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method changes the operational state of a VNF instance.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/operate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdChangeExtConn(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method changes the external connectivity of a VNF instance.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/change_ext_conn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdChangeVnfpkg(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method changes the current VNF package on which the VNF instance is based.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/change_vnfpkg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdCreateSnapshot(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method requests tacking a VNF instance snapshot and populating a previously created VNF sn</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/create_snapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfInstancesVnfInstanceIdRevertToSnapshot(vnfInstanceId, body, callback)</td>
    <td style="padding:15px">The POST method requests reverting a VNF/VNFC instance to a VNF/VNFC snapshot.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_instances/{pathv1}/revert_to_snapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfLcmOpOccs(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">The client can use this method to query status information about multiple VNF lifecycle management</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_lcm_op_occs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfLcmOpOccsVnfLcmOpOccId(vnfLcmOpOccId, callback)</td>
    <td style="padding:15px">The client can use this method to retrieve status information about a VNF lifecycle management oper</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_lcm_op_occs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfLcmOpOccsVnfLcmOpOccIdRetry(vnfLcmOpOccId, callback)</td>
    <td style="padding:15px">The POST method initiates retrying a VNF lifecycle operation if that operation has experienced a te</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_lcm_op_occs/{pathv1}/retry?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfLcmOpOccsVnfLcmOpOccIdRollback(vnfLcmOpOccId, callback)</td>
    <td style="padding:15px">The POST method initiates rolling back a VNF lifecycle operation if that operation has experienced</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_lcm_op_occs/{pathv1}/rollback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfLcmOpOccsVnfLcmOpOccIdFail(vnfLcmOpOccId, callback)</td>
    <td style="padding:15px">The POST method marks a VNF lifecycle management operation occurrence as "finally failed" if that o</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_lcm_op_occs/{pathv1}/fail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfLcmOpOccsVnfLcmOpOccIdCancel(vnfLcmOpOccId, callback)</td>
    <td style="padding:15px">The POST method initiates cancelling an ongoing VNF lifecycle operation while it is being executed</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_lcm_op_occs/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVnfSnapshots(body, callback)</td>
    <td style="padding:15px">The POST method creates a new individual VNF snapshot resource.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfSnapshots(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">The GET method queries information about multiple VNF/VNFC snapshots.
</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVnfSnapshotsVnfSnapshotInfoId(vnfSnapshotInfoId, callback)</td>
    <td style="padding:15px">The GET method retrieves information about a VNF /VNFC snapshot by reading an individual VNF snapsh</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVnfSnapshotsVnfSnapshotInfoId(vnfSnapshotInfoId, callback)</td>
    <td style="padding:15px">This method deletes an individual VNF snapshot resource and the associated VNF snapshot information</td>
    <td style="padding:15px">{base_path}/{version}/vnflcm/v1/vnf_snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmJobs(filter, allFields, fields, excludeFields, excludeDefault, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">The client can use this method to retrieve information about PM jobs.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/pm_jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPmJobs(body, callback)</td>
    <td style="padding:15px">The POST method creates a PM job. As the result of successful executing this method, a new  "Indivi</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/pm_jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmJobsPmJobId(pmJobId, callback)</td>
    <td style="padding:15px">The client can use this method for reading an individual PM job.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/pm_jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePmJobsPmJobId(pmJobId, callback)</td>
    <td style="padding:15px">This method terminates an individual PM job. As the result of successfully executing this method, t</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/pm_jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPmJobsPmJobId(pmJobId, body, callback)</td>
    <td style="padding:15px">This method allows to modify an "individual PM job" resource. This method shall follow the provisio</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/pm_jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPmJobsPmJobIdReportsReportId(pmJobId, reportId, callback)</td>
    <td style="padding:15px">The client can use this method for reading an individual performance report.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/pm_jobs/{pathv1}/reports/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholds(filter, nextpageOpaqueMarker, callback)</td>
    <td style="padding:15px">The client can use this method to query information about thresholds.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postThresholds(body, callback)</td>
    <td style="padding:15px">The POST method can be used by the client to create a threshold. As the result of successfully exec</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholdsThresholdId(thresholdId, callback)</td>
    <td style="padding:15px">The client can use this method for reading an individual threshold.
</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteThresholdsThresholdId(thresholdId, callback)</td>
    <td style="padding:15px">This method allows to delete a threshold. As the result of successfully executing this method, the
</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchThresholdsThresholdId(thresholdId, body, callback)</td>
    <td style="padding:15px">This method allows to modify an "Individual threshold" resource. This method shall follow the provi</td>
    <td style="padding:15px">{base_path}/{version}/vnfpm/v2/thresholds/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
