# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Etsi_sol002 System. The API that was used to build the adapter for Etsi_sol002 is usually available in the report directory of this adapter. The adapter utilizes the Etsi_sol002 API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The ETSI Sol002 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with any system using the ETSI Sol002 standard. With this adapter you have the ability to perform operations such as:

- Alarms
- Thresholds
- Configuration

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
