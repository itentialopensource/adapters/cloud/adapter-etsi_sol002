# ETSI Standard sol002

Vendor: ETSI
Homepage: https://www.etsi.org/

Product: ETSI Standard sol002
Product Page: https://www.etsi.org/deliver/etsi_gs/NFV-SOL/001_099/002/03.05.01_60/gs_NFV-SOL002v030501p.pdf

## Introduction
We classify ETSI Sol002 into the Cloud domain.

## Why Integrate
The ETSI Sol002 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with any system using the ETSI Sol002 standard. With this adapter you have the ability to perform operations such as:

- Alarms
- Thresholds
- Configuration

## Additional Product Documentation
The [API documents for ETSI](https://nfvwiki.etsi.org/index.php?title=API_specifications)
The [API documents for ETSI Sol002](https://www.etsi.org/deliver/etsi_gs/NFV-SOL/001_099/002/)