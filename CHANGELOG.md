
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:11PM

See merge request itentialopensource/adapters/adapter-etsi_sol002!12

---

## 0.4.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-etsi_sol002!10

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:20PM

See merge request itentialopensource/adapters/adapter-etsi_sol002!9

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:33PM

See merge request itentialopensource/adapters/adapter-etsi_sol002!8

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol002!7

---

## 0.3.4 [03-28-2024]

* Changes made at 2024.03.28_13:15PM

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol002!6

---

## 0.3.3 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol002!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:31PM

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol002!4

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:43AM

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol002!3

---

## 0.3.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol002!2

---

## 0.2.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-etsi_sol002!1

---

## 0.1.2 [03-02-2022] & 0.1.1 [03-02-2022]

- Initial Commit

See commit 75f0a6d

---
